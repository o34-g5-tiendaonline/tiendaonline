function loadData(){ //consultar todo
    let request = sendRequest('productos/list', 'GET', '');//variable
    let table = document.getElementById('products-table');
    console.log(request)
    table.innerHTML = "";
    request.onload = function(){
        console.log(request.response);
        let data = request.response;
        console.log(data);
        data.forEach((element, index) => {
            console.log("CONTENIDO",element)
            table.innerHTML += 
            `
                <tr>
                    <th>${element.idProducto}</th>
                    <td>${element.nombre}</td>
                    <td>${element.tipo}</td>
                    <td>${element.marca}</td>
                    <td>${element.codigo}</td>
                    <td>${element.costo}</td>
                    <td>${element.precioVenta}</td>
                    
                    <td>
                        <button type="button" class= "btn btn-primary" 
                        onclick='window.location = "form_productos.html?id=${element.idProducto}"'>
                        Editar
                        </button>
                        <button type="button" class= "btn btn-danger"
                        onclick='deleteProducto(${element.idProducto})'>
                        Eliminar
                        </button>
                    </td>
                </tr> 
            `
        });
    }
    request.onerror = function(){
        table.innerHTML = 
        `
        <tr>
            <td colspan = "6 "> Error </td>
        </tr>
        `;
        
    }
}

function saveProducto(){ //agregar
    let id = document.getElementById('product-id').value;
    let name = document.getElementById('product-name').value;
    let purchase = document.getElementById('product-purchase-value').value;
    let sale = document.getElementById('product-sale-value').value;    
    let code = document.getElementById('product-cod').value;
    let mark = document.getElementById('product-mark').value;
    let type = document.getElementById('product-type').value;
    console.log("que id trae", id)

    let data = {'idProducto': id, 'nombre': name , 'tipo': type, 'marca': mark , 'codigo': code, 'costo': purchase, 'precioVenta': sale}
    
    let request = sendRequest('productos/', id ? 'PUT' : 'POST', data); 
    
    request.onload = function(){
        window.location = 'productos.html';
    }
    request.onerror = function(){
        alert('Error al guardar')
        console.log("error", request.onerror)
    }
}

function deleteProducto(idProducto){ //borrar
    let request = sendRequest('productos/'+idProducto,'DELETE','')
    request.onload = function() {
        window.location = 'productos.html';
        //loadData()
    }
}

function loadProducto(idProducto){//consultar por id

    let request = sendRequest('productos/list/'+idProducto,'GET', ''); 
    let id = document.getElementById('product-id');
    let name = document.getElementById('product-name');
    let purchase = document.getElementById('product-purchase-value');
    let sale = document.getElementById('product-sale-value');    
    let code = document.getElementById('product-cod');
    let mark = document.getElementById('product-mark');
    let type = document.getElementById('product-type');


    request.onload = function(){
        let data = request.response
        console.log("valores que trae data", data)
        id.value = data.idProducto
        name.value = data.nombre
        purchase.value = data.costo
        sale.value = data.precioVenta
        code.value = data.codigo
        mark.value = data.marca
        type.value = data.tipo


    }
    request.onerror = function(){
        alert('Error al editar')
        console.log("error", request.onerror)
    }
}
