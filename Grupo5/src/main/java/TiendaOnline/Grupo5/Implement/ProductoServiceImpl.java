/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package TiendaOnline.Grupo5.Implement;
//import com.misiontic.proyectociclo3.Models.Producto;
import TiendaOnline.Grupo5.Models.Producto;
//import com.misiontic.proyectociclo3.Dao.ProductoDao;
import TiendaOnline.Grupo5.Dao.ProductoDao;
//import com.misiontic.proyectociclo3.Service.ProductoService;
import TiendaOnline.Grupo5.Service.ProductoService;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
 
@Service
public class ProductoServiceImpl implements ProductoService {
    @Autowired //Ayuda con la conexion de los servicios
    private ProductoDao ProductoDao;
    
    @Override
    @Transactional(readOnly = false)
    public Producto save(Producto producto){
        return ProductoDao.save(producto);
    }
    
    @Override
    @Transactional(readOnly = false)
    public void delete(Integer id){
        ProductoDao.deleteById(id);
    }
    
    @Override
    @Transactional(readOnly = true)
    public List<Producto> findAll(){
        return (List<Producto>) ProductoDao.findAll();
    }
    
    @Override
    @Transactional(readOnly = true)
    public Producto findById(Integer id){
        return ProductoDao.findById(id).orElse(null);
    }
}
