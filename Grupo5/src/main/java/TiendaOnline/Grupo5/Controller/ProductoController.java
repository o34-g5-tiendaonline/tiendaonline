/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package TiendaOnline.Grupo5.Controller;

//import com.misiontic.proyectociclo3.Models.Producto;
import TiendaOnline.Grupo5.Models.Producto;
//import com.misiontic.proyectociclo3.Models.Transaccion;
//import com.misiontic.proyectociclo3.Service.ProductoService;
import TiendaOnline.Grupo5.Service.ProductoService;
//import com.misiontic.proyectociclo3.Service.TransaccionService;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.PutMapping;


@RestController
@CrossOrigin("*")
@RequestMapping("/productos") //pendiente de si es producto o productos

public class ProductoController {
    @Autowired
    private ProductoService productoService;
    
    @PostMapping(value="/") //para actualizar
    public ResponseEntity<Producto>agregar(@RequestBody Producto producto){
        Producto obj = productoService.save(producto);
        return new ResponseEntity<>(obj,HttpStatus.OK);            
    }
    
    @DeleteMapping(value = "/{id}")
    public ResponseEntity<Producto> eliminar(@PathVariable Integer id){
        Producto obj = productoService.findById(id);
        if(obj!=null){
            productoService.delete(id);            
        }else{
            return new ResponseEntity<>(obj, HttpStatus.INTERNAL_SERVER_ERROR);
        }
        return new ResponseEntity<>(obj,HttpStatus.OK); 
    }
    
    @PutMapping(value = "/")
    public ResponseEntity<Producto>editar(@RequestBody Producto producto){
        Producto obj = productoService.findById(producto.getIdProducto());
        if(obj != null){
            obj.setNombre(producto.getNombre());
            obj.setTipo(producto.getTipo());             
            obj.setMarca(producto.getMarca());
            obj.setCodigo(producto.getCodigo());
            obj.setCosto(producto.getCosto());
            obj.setPrecioVenta(producto.getPrecioVenta());
            
            productoService.save(obj);
            
        }else{
           return new ResponseEntity<>(obj, HttpStatus.INTERNAL_SERVER_ERROR);
        }
           return new ResponseEntity<>(obj,HttpStatus.OK); 
    }
    
    @GetMapping("/list")
    public List<Producto> consultarTodo(){
        return productoService.findAll();
    }
    
    @GetMapping("/list/{id}")
    public Producto consultarPorId(@PathVariable Integer id){
        return productoService.findById(id);        
    }
    
}
