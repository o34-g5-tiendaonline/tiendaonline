/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package TiendaOnline.Grupo5.Models;

import java.io.Serializable;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import org.hibernate.annotations.Generated;
import org.hibernate.annotations.GenericGenerator;


@Table
@Entity(name = "productos")

/* @author Jaime */
  
public class Producto implements Serializable{
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)//indicar que es PK
    @Column(name = "idProductos")
    private Integer idProducto;
    
    @Column(name = "Nombre")
    private String Nombre;
    
    @Column(name = "Tipo")
    private String Tipo;
    
    @Column(name = "Marca")
    private String Marca;      
    
    @Column(name = "Codigo")
    private String Codigo;   
        
    @Column(name = "Costo")
    private Integer Costo;    
    
    @Column(name = "PrecioVenta")
    private Integer PrecioVenta;    
    
    
    //GETTERS AND SETTERS

    public Integer getIdProducto() {
        return idProducto;
    }

    public void setIdProducto(Integer idProducto) {
        this.idProducto = idProducto;
    }

    public String getNombre() {
        return Nombre;
    }

    public void setNombre(String Nombre) {
        this.Nombre = Nombre;
    }

    public String getTipo() {
        return Tipo;
    }

    public void setTipo(String Tipo) {
        this.Tipo = Tipo;
    }


    public String getMarca() {
        return Marca;
    }

    public void setMarca(String Marca) {
        this.Marca = Marca;
    }

    public String getCodigo() {
        return Codigo;
    }

    public void setCodigo(String Codigo) {
        this.Codigo = Codigo;
    }

    public Integer getCosto() {
        return Costo;
    }

    public void setCosto(Integer Costo) {
        this.Costo = Costo;
    }

    public Integer getPrecioVenta() {
        return PrecioVenta;
    }

    public void setPrecioVenta(Integer PrecioVenta) {
        this.PrecioVenta = PrecioVenta;
    }
   
    
}
