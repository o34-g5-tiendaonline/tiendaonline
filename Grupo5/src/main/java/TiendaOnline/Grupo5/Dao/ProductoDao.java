/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Interface.java to edit this template
 */
package TiendaOnline.Grupo5.Dao;
//import com.misiontic.proyectociclo3.Models.Producto;
import TiendaOnline.Grupo5.Models.Producto;
import org.springframework.data.repository.CrudRepository;

public interface ProductoDao extends CrudRepository <Producto, Integer> {
    
    
}
