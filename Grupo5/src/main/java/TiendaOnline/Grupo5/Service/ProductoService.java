/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Interface.java to edit this template
 */
package TiendaOnline.Grupo5.Service;
//import com.misiontic.proyectociclo3.Models.Producto;
import TiendaOnline.Grupo5.Models.Producto;
import java.util.List;

public interface ProductoService {
    //METODOS DEL CRUD DE LA TABLA PRODUCTO
    public Producto save(Producto producto);
    //public Producto(clase) save(Producto = ser refiere al modelo; producto = tabla);
    public void delete(Integer id);
    public Producto findById(Integer id);
    public List<Producto> findAll();
}
